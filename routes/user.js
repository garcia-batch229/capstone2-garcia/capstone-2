const express = require("express");
const router = express.Router();
const userControllers = require("../controllers/userControllers.js")
const auth = require("../auth.js")


//User registration
router.post("/register", (req, res) => {

	userControllers.registerUser(req.body)
	.then(resultFromController => res.send(resultFromController));
})

//All user
router.get("/all-user", (req,res) => {
	userControllers.allUser().then(resultFromController => res.send(resultFromController));
})

//Get admin
router.get("/all-admin", (req,res) => {
	userControllers.getAdmin().then(resultFromController => res.send(resultFromController));
})

//Login user
router.post("/login", (req,res) => {
	userControllers.loginUser(req.body).then(resultFromController => res.send(resultFromController));
})

//Admin user w/ token verification

router.put("/:userId/admin", auth.verify,(req,res) => {

	const data = auth.decode(req.headers.authorization);

	if(data.isAdmin == true){
		userControllers.userAdmin(req.params, req.body).then(resultFromController => res.send(resultFromController));
	}else {
		res.send("User is not an Admin");
	}
})

//User Checkout (Non-admin only)
// router.post("/checkout", auth.verify, (req,res) => {

// 	/*let data = {
// 		userId: auth.decode(req.headers.authorization).id,
// 		productId: req.body.productId
// 	};*/
// 	const admin = auth.decode(req.headers.authorization);
	

// 	if(admin == false){
// 		userControllers.checkoutUser(data).then(resultFromController => res.send(resultFromController));
// 	}else{
// 		res.send("Admin user is not allowed to checkout");
// 	}
// })


router.post("/checkout", auth.verify,(req,res) => {

	

	let data = {
		userId: auth.decode(req.headers.authorization).id,
		productId: req.body.productId,
		orderId : req.body.productId,
		isAdmin: auth.decode(req.headers.authorization).isAdmin,
		totalAmount: req.body.totalAmount
	}

	if(data.isAdmin == false){
		userControllers.checkoutUser(data).then(resultFromController => res.send(resultFromController));
	}else {
		res.send("Admin user is not allowed to checkout");
	}
})



//User details
router.get("/userDetails", (req,res) => {
	userControllers.userDetails(req.body).then(resultFromController => res.send(resultFromController));
})

//All Orders(Admin only)
router.get("/allOrders", auth.verify, (req,res) => {
	if(auth.decode(req.headers.authorization).isAdmin == true){
		userControllers.allOrder().then(resultFromController => res.send(resultFromController));
	}else{
		res.send("User is not an Admin");
	}
})


//User Orders(Non-admin) - using req.body
router.get("/userOrders", auth.verify, (req,res) => {

	let data = {
		isAdmin: auth.decode(req.headers.authorization).isAdmin,
			}
	if(data.isAdmin == false){
		userControllers.userOrder(req.body).then(resultFromController => res.send(resultFromController));
	}else{
		res.send("Access Denied - not a user");
	}
})

//User Orders(Non-admin) - using wildcard ":"
router.get("/:userId/userOrders", auth.verify, (req,res) => {

	let data = {
		isAdmin: auth.decode(req.headers.authorization).isAdmin,
			}
	if(data.isAdmin == false){
		userControllers.userOrderId(req.params).then(resultFromController => res.send(resultFromController));
	}else{
		res.send("Access Denied - not a user");
	}
})

//This is to connect the route to other modules
module.exports = router;