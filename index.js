const express = require("express");
const mongoose = require("mongoose");
const cors = require("cors");
const userRoutes = require("./routes/user.js");
const productRoutes = require("./routes/product.js");
const app = express();

//MongoDB Connection
mongoose.connect("mongodb+srv://admin:admin1234@cluster0.ic1qfxr.mongodb.net/Capstone-2?retryWrites=true&w=majority",{
	useNewUrlParser : true,
	useUnifiedTopology: true
});

mongoose.set('strictQuery', false);
let db = mongoose.connection;


db.on("error", console.error.bind(console, "connection error"));

db.once("open", () => console.log("We're connected to the cloud database."));

//Middlewares
app.use(express.json());
app.use(express.urlencoded({extended:true}));
app.use("/users", userRoutes);
app.use("/products", productRoutes);

app.listen(process.env.PORT || 4000, () => {
	console.log(`Ecommerce Api is now online on port ${process.env.PORT || 4000}`)
});