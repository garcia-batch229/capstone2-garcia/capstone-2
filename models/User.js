const mongoose = require("mongoose");


const userSchema = new mongoose.Schema({
	username: {
		type: String,
		required: [true, "Username is required"]
	},
	email: {
		type: String,
		required: [true, "Email is required!"]
	},
	password: {
		type: String,
		required: [true, "Password is required!"]
	},
	isAdmin: {
		type: Boolean,
		default: false
	},
	registerDate: {
		type: Date,
		default: new Date()
	},
	orders: [{
		productId: {
			type: String,
			required: [true, "Product ID is requried!"]
		},
		totalAmount: {
			type: String,
			required: [true, "Amount is required!"]
		},
		purchasedOn: {
			type: Date,
			default: new Date()
		}
	}]
		
})




module.exports = mongoose.model("User", userSchema);