const User = require("../models/User.js");
const Product = require("../models/Product.js");
const auth = require("../auth.js");
const bcrypt = require("bcrypt");
const user = require("../routes/user.js");




//User registration
	/*
		1. Create a new user object using mongoose model
		2. Check User.email if already used
		3. Check User.username if already used
		3. Make the password encrypted
		4. Save the new User to the database
	*/

module.exports.registerUser = (reqBody) => {
	return User.findOne({email:{$regex:reqBody.email,$options:reqBody.email}}).then((result) => {
		if(result != null){
			return [false, "Email already used"];
		}else{
			return User.findOne({username:{$regex:reqBody.username,$options:reqBody.username}}).then((result) => {
				if(result != null){
					return [false, "Username already used"];
				}else{
					let newUser = new User({
						username: reqBody.username,
						email: reqBody.email,
						password: bcrypt.hashSync(reqBody.password, 10)
					})
					return newUser.save().then((result,error) => {
						if(error){
							return false;
						}else{
							console.log(newUser);
							return [true, "User has been successfully registered!"]
						}
					})
				}
			})
		}
	})
}	

//All User
	/*
		1. Show all registered user
	*/

module.exports.allUser = () => {
	return User.find({isAdmin: false}).then(result => {
		return result;
	})
}

//Admin user

module.exports.getAdmin = () => {
	return User.find({isAdmin: true}).then(result => {
		return result;
	})
}

/* User Login w/ (Authentication or "create token")
	
	1. Check the database if user email exists
	2. Compare the password that the user provided with password stored in the database.
	3. Create token or Generate a JSON web tokken if successfully login

*/

module.exports.loginUser = (reqBody) => {
	return User.findOne({email:{$regex:reqBody.email,$options:reqBody.email}}).then(
		result => {
			if(result == null){
				return [false, "User email is not registered"]
			}else{
				//compare password with database and user provided
				const isPasswordCorrect = bcrypt.compareSync(reqBody.password, result.password);
				if(isPasswordCorrect){
					return {access: auth.createAccessToken(result)}
				}else {
					return [false, "Password does not match"]
				}
			}
		})
}

/* User Admin

	1. Create a variable "adminStatus" which will store the info in reqBody
	2. Find and Update the user from the reqParams and adminStatus
*/

module.exports.userAdmin = (reqParams,reqBody) => {

	//Update the admin status to "true/false"
	let status = {
		isAdmin: reqBody.isAdmin
	}

	return User.findByIdAndUpdate(reqParams.userId,status).then((result,error) => {
		
		if(error){
			return false;
		}else{

			return [true, "Status changed successfully"];
		}
	})
}


//User details
//Get Profile
/*
	Steps:
	1. Find the document in the databse using the User's ID.
	2. Reassign the password of the returned document to an empty string
	3. Return the result back to the frontend

*/


module.exports.userDetails = (reqBody) => {
	return User.findOne({username: reqBody.username}).then(result => {

		if(result == null) {
			return "Invalid username"
		}else{
			//To avoid showing password details
		result.password = "";
		return result;
		}
		
	})
}


//User Checkout


module.exports.checkoutUser = async (data) => {



		let isUserUpdated = await User.findById(data.userId).then(user => {
			console.log(user);
			//adds the courseId to the user's enrollments array	
			user.orders.push({productId: data.productId, totalAmount: data.totalAmount});

			//saves the updated user information in the database
			return user.save().then((user,error) => {
				//if failed
				if(error){
					return false;

				//if succesful	
				}else {
					return true;
				}
			});
		});

		//adds the userId in the enrollees array of the course
		//await - to allow the enroll method to complete updating the course before returning a response to the frontend
		let isProductUpdated = await Product.findById(data.productId).then(product => {

			//add the user id in course's enrollees array
			product.orders.push({orderId: data.userId});

			//saves the updated course info in the database
			return product.save().then((course,error) => {
				//if failed
				if(error){
					return false;
				//if successful	
				}else{
					return true;
				}
			});
		});

		if(isUserUpdated && isProductUpdated) {
			return "Successfully updated";
		}else {
			return "Request unsuccessful";
		}
	}


//All Orders (Admin Only)
module.exports.allOrder = () => {
	return User.find({isAdmin: false},{_id:0,username:1,orders:1}).then(result => {
		return result;
	})
}


//User's Order
module.exports.userOrder = (reqBody) => {
	return User.find({username: reqBody.username},{_id:0,username:1,orders:1}).then(result => {
		return result;
		
	})
}

module.exports.userOrderId = (reqParams) => {
	return User.findById(reqParams.userId,{_id:0,username:1,orders:1}).then(result => {
		return result;
		
	})
}